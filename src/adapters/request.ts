import fetch from "node-fetch";
import {
	TaskData,
	TaskDataResponse,
	RequestTasks,
	RequestStatus,
	ProjectsData,
	ProjectsDataResponse,
	RequestProjects,
} from "../controllers/interfaces";

async function _fetchFromAPI(
	url: string,
	userParams?: { [key: string]: string }
) {
	const statusesUrl = `${process.env.API_URL_BASE}/${url}`;
	let requestParams = {
		lang: "eng",
		company_account_id: process.env.COMPANY_ACCOUNT_ID || "",
		apiKey: process.env.API_KEY || "",
	};

	if (userParams) {
		requestParams = { ...requestParams, ...userParams };
	}

	const params = {
		method: "POST",
		headers: {
			"content-type": "application/json",
		},
		body: JSON.stringify(requestParams),
	};

	const response = await fetch(statusesUrl, params);
	if (!response.ok) {
		return {
			status: "error",
			error: "Error loading data from api",
		};
	}

	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	const result: any = await response.json();
	return result.data;
}

export const reguestTask: RequestTasks = async () => {
	const received = await _fetchFromAPI("tasks/list");
	const tasks: Array<TaskData> = received.map((elem: TaskData) => {
		return {
			datetime_completed: elem.datetime_completed,
			status: elem.status,
			event_name: elem.event_name,
			event_id: elem.event_id,
			sortorder: elem.sortorder,
		};
	});
	return { status: RequestStatus.ok, data: tasks } as TaskDataResponse;
};

export const reguestProjects: RequestProjects = async () => {
	const received = await _fetchFromAPI("projects/list");
	const projects: Array<ProjectsData> = received.map((elem: ProjectsData) => {
			return {
                project_id: elem.project_id,
                no: elem.no,
                project_name: elem.project_name,
                color: elem.color,
                status: elem.status,
			};
	});
	return { status: RequestStatus.ok, data: projects } as ProjectsDataResponse;
};
