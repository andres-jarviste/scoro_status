/* eslint-disable new-cap */
import { Get, Route } from "tsoa";
import { ProjectsData, ProjectsDataResponse } from "./interfaces";

import { reguestProjects } from "../adapters/request";

interface StatusResponse {
	data: Array<ProjectsData>;
}

@Route("api/projects")
export default class ProjectsController {
	@Get("/")
	public async getProjects(): Promise<StatusResponse> {
		const projects: ProjectsDataResponse = await reguestProjects();
		return { data: projects.data };
	}
}
