export const enum requestType {
    Status = "Status",
    Task = "Task",
    Project = "Project",
}

export type statusRequestParams = {
    filter: ['tasks', 'projects' ]
}

export type TaskData = {
    datetime_completed: string,
    status: string,
    event_name: string,
    event_id: string,
    sortorder: number   
}

export type TaskDataResponse = {
    status: RequestStatus,
    data: Array<TaskData>
}

export type ProjectsData = {
    project_id: string,
    no: string,
    project_name: string,
    color: string,
    status: string
}
export type ProjectsDataResponse = {
    status: RequestStatus,
    data: Array<ProjectsData>
}

export enum RequestStatus {
    ok = 'ok',
    error = 'error'
}


export interface RequestTasks {
    () : Promise<TaskDataResponse>
}

export interface RequestProjects {
    () : Promise<ProjectsDataResponse>
}



