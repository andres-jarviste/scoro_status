/* eslint-disable new-cap */
import { Get, Route } from "tsoa";
import { TaskData, TaskDataResponse } from "./interfaces";

import { reguestTask } from "../adapters/request";

const _tasksCache: { [key: string]: Array<TaskData> } = {};

interface StatusResponse {
	data: Array<TaskData>;
}

@Route("api/tasks")
export default class TasksController {
	@Get("/")
	public async getTasks(): Promise<StatusResponse> {
		const cacheKey = "all";
		if (_tasksCache[cacheKey]) {
			return { data: _tasksCache[cacheKey] };
		}
		const tasks: TaskDataResponse = await reguestTask();
		_tasksCache[cacheKey] = tasks.data;
		return { data: tasks.data };
	}
}
