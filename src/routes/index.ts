import express, {Request, Response} from "express";
import StatusController from "../controllers/status";
import TasksController from "../controllers/tasks"
import ProjectsController from "../controllers/projects"

// eslint-disable-next-line new-cap
const router = express.Router();

router.get('/status', async (req: Request, res: Response) => {
    const controller = new StatusController();
    const response = await controller.getStatusData(<string>req.query.filter);
    return res.send(response)
})
router.get('/tasks', async (req: Request, res: Response) => {
    const controller = new TasksController();
    const response = await controller.getTasks();
    return res.send(response)
})
router.get('/projects', async (req: Request, res: Response) => {
    const controller = new ProjectsController();
    const response = await controller.getProjects();
    return res.send(response)
})

export default router
