# Backend component of Status Component Project

This backend component reads status data from Scoro API and provides it to front-end over the api.

Backend loads currently status data and is able to search statuses by modules (e.g. projects, tasks, etc).

Module publishes its API in /docs endpoint with the help of automaticly generated OpenAPI specification of endpoints.

Backend component is implemented in NodeJS with Typescript using Express library

Backend is deployed to Heroku platform. Documentation of the available endpoint(s) is available [here](https://ancient-sierra-95788.herokuapp.com/docs/)

## Running in local environment
For running in local environment copy file .env.example to file .env and fill in credentials.

Install node dependencies
```
npm install
```
Run in local development server
```
npm run dev
```

## Deploying to production
Build production code from Typescript source
```
npm run build
```

Prepare front end source code and copy it to folder /public.
In [front end repository](https://gitlab.com/andres-jarviste/scoro_status_client) is a script that automates that process.

review and commit all the code. To deploy the code to Heroku you have to be logged in to Heroku account and connected to current app. Then issue following command
```
git push heroku main
```
